#include <stdio.h>
#include "licel.h"
#include "medianfilter.h"

int main(int argc, char *argv[])
{
  struct LicelFile f;
  if (argc==2)
    {
      readLicelFile(argv[1], &f);
      
      for(int i=0; i<f.nDatasets; i++)
	filterDataset(&f, i);
      
      writeLicelFile(&f);
    }
  else
    fprintf(stderr, "Usage: %s licelFile \n", argv[0]);
}
