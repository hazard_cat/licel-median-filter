#ifndef __LICEL_H__
#define __LICEL_H__

#include <time.h>

#define LICEL_HEADER_LEN 81
#define LICEL_SITE_STR 9
#define LICEL_MAX_CHANNELS 12
#define MAX_LENGTH 16380

struct LicelProfile {
  int isActive;
  int isPhoton;
  int laserType;
  int nDataPoints;
  char reserved1; //1
  int highVoltage;
  float binWidth;
  float wavelength;
  char polarization;
  char reserved2; // 0
  char reserved3; // 0
  int binShift;
  int decBinShift;
  int adcBits;
  int nShots;
  float discrLevel;
  char deviceId[3];
  int nCrate;
  int data[MAX_LENGTH];
};

struct LicelFile {
  char measurementSite[LICEL_SITE_STR];
  time_t measurementStartTime;
  time_t measurementStopTime;
  double altitudeAboveSeaLevel;
  double longitude;
  double latitude;
  double zenith;

  int laser1NShots;
  int laser1Freq;
  int laser2NShots;
  int laser2Freq;
  int nDatasets;
  int laser3NShots;
  int laser3Freq;

  struct LicelProfile dataset[LICEL_MAX_CHANNELS];

};

int readLicelFile(const char *fname, struct LicelFile *data);
int writeLicelFile(struct LicelFile *data);
int filterDataset(struct LicelFile *data, int chNum);
void printChannel(struct LicelFile *data, int ch);
#endif
