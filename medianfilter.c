#include "medianfilter.h"
#include <string.h>

void sort(int *data, int len)
{

  for(int i=0; i<len-1; i++)
    {
      int min = i;
      for(int j=i+1; j<len; j++)
	if(data[j]<data[min])
	  min = j;
      const int temp = data[i];
      data[i] = data[min];
      data[min] = temp;
    }
}

int median(int *data, int len)
{
  sort(data, len);
  int idx = len/2;

  return data[idx];
}

void medfilt1(const int *indata, int *outdata, int size)
{

  memcpy(outdata, indata, sizeof(int)*size);
  
  for(int i=1; i<size-1; i++)
    {
      int window[3];

      for(int j=0; j<3; j++)
	window[j] = indata[i-1+j];

      outdata[i-1] = median(window, 3);
      
    }
}


