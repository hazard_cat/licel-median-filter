#include <stdio.h>
#include "licel.h"
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "medianfilter.h"

int readLicelFile(const char *fname, struct LicelFile *data)
{
  FILE *file;
  if((file=fopen(fname,"r"))!=NULL)
    {
      char tmp[LICEL_HEADER_LEN];
      
      // первая строка файла
      fgets(tmp, LICEL_HEADER_LEN, file);
      // вторая строка файла
      fgets(tmp, LICEL_HEADER_LEN, file);
      
      char b[20], c[20], d[20], e[20], ff[LICEL_HEADER_LEN];
      
      int n = sscanf(tmp+1, "%s %s %s %s %s %lf %lf %lf %lf", data->measurementSite,
		     b, c, d, e,
		     &data->altitudeAboveSeaLevel,
		     &data->longitude,
		     &data->latitude,
		     &data->zenith);

      if(n!=9)
	{
	  fprintf(stderr,"ERROR  parsing second line of a file %s\n",fname);
	  exit(1);
	}
      
      struct tm time;

      // преобразуем время начала измерений в удобоваримый формат
      sprintf(ff, "%s %s", b, c);
      strptime(ff, "%d/%m/%Y %H:%M:%S", &time);
      data->measurementStartTime = mktime(&time);

      // преобразуем время конца измерений в удобоваримый формат
      sprintf(ff, "%s %s", d, e);
      strptime(ff, "%d/%m/%Y %H:%M:%S", &time);
      data->measurementStopTime = mktime(&time);
      
      // третья строка файла
      fgets(tmp, LICEL_HEADER_LEN, file);

      n = sscanf(tmp+1, "%d %d %d %d %d %d %d", &data->laser1NShots,
		 &data->laser1Freq,
		 &data->laser2NShots,
		 &data->laser2Freq,
		 &data->nDatasets,
		 &data->laser3NShots,
		 &data->laser3Freq);
      
      if(n!=7)
	{
	  fprintf(stderr,"ERROR  parsing third line %d of a file %s\n", n, fname);
	  exit(1);
	}

      for(int i=0; i<data->nDatasets; i++)
	{
	  fgets(tmp, LICEL_HEADER_LEN, file);
	  n = sscanf(tmp+1, "%d %d %d %d %c %d %f %05f.%c %c %c %02d %03d %02d %06d %f %c%c%1d",
		     &data->dataset[i].isActive,
		     &data->dataset[i].isPhoton,
		     &data->dataset[i].laserType,
		     &data->dataset[i].nDataPoints,
		     &data->dataset[i].reserved1,
		     &data->dataset[i].highVoltage,
		     &data->dataset[i].binWidth,
		     &data->dataset[i].wavelength,
		     &data->dataset[i].polarization,
		     &data->dataset[i].reserved2,
		     &data->dataset[i].reserved3,
		     &data->dataset[i].binShift,
		     &data->dataset[i].decBinShift,
		     &data->dataset[i].adcBits,
		     &data->dataset[i].nShots,
		     &data->dataset[i].discrLevel,
		     &data->dataset[i].deviceId[0],
		     &data->dataset[i].deviceId[1],
		     &data->dataset[i].nCrate
		     );
	  data->dataset[i].deviceId[2]=0;
	  
	}

      printf("Read %d %ld\n", n, data->measurementStopTime-data->measurementStartTime);
      
      // читаем строку-разделитель
      fgets(tmp, LICEL_HEADER_LEN, file);
      //printf("<%d><%d>\n",tmp[0],tmp[1]);
      //читаем данные каналов
      for(int i=0; i<data->nDatasets; i++)
	{
	  n = fread((char*)data->dataset[i].data, sizeof(float), data->dataset[i].nDataPoints, file);
	  fgets(tmp, LICEL_HEADER_LEN, file);
	  //printf("<%d><%d>\n",tmp[0],tmp[1]);
	  //printf("Read = %d data[100] = %d\n", n, data->dataset[i].data[100]);
	}
      
      //printf("Latitude = %lf \n", data->latitude);
      fclose(file);
      return 0;
    }
  else
    return 1;
}

int writeLicelFile(struct LicelFile *data)
{
  FILE *file;

  char tmp[LICEL_HEADER_LEN];
  struct tm *local;
  local = localtime(&data->measurementStartTime);
  sprintf(tmp,"b%02d%1x%02d%02d.%02d%02d%02d",
	  local->tm_year+1900 - 2000,
	  local->tm_mon+1,
	  local->tm_mday,
	  local->tm_hour,
	  local->tm_min,
	  local->tm_sec,
	  0
	  );
  
  printf("%s%c%c",tmp,0xd,0xa);

  if((file=fopen(tmp, "w"))!=NULL)
    {
      // строка 1
      fprintf(file, " %s%c%c", tmp, 0xd, 0xa);

      char a[20], b[20];
      strftime(a, 20, "%d/%m/%Y %H:%M:%S", local);

      local = localtime(&data->measurementStopTime);
      strftime(b, 20, "%d/%m/%Y %H:%M:%S", local);

      // строка 2
      
      fprintf(file, " %s %s %s %04.0f %06.1f %06.1f %02.0f%c%c",
	      data->measurementSite,
	      a,
	      b,
	      data->altitudeAboveSeaLevel,
	      data->longitude,
	      data->latitude,
	      data->zenith,
	      0xd,
	      0xa
	      );
      

      // строка 3
      fprintf(file, " %07d %04d %07d %04d %02d %07d %04d%c%c",
	      data->laser1NShots,
	      data->laser1Freq,
	      data->laser2NShots,
	      data->laser2Freq,
	      data->nDatasets,
	      data->laser3NShots,
	      data->laser3Freq,
	      0xd,
	      0xa
	      );

      // пишем описание каналов
      for(int i=0; i<data->nDatasets; i++)
	{
	  fprintf(file, " %01d %01d %01d %05d %c %04d %4.2f %05.0f.%c %c %c %02d %03d %02d %06d ",
		  data->dataset[i].isActive,
		  data->dataset[i].isPhoton,
		  data->dataset[i].laserType,
		  data->dataset[i].nDataPoints,
		  data->dataset[i].reserved1,
		  data->dataset[i].highVoltage,
		  data->dataset[i].binWidth,
		  data->dataset[i].wavelength,
		  data->dataset[i].polarization,
		  data->dataset[i].reserved2,
		  data->dataset[i].reserved3,
		  data->dataset[i].binShift,
		  data->dataset[i].decBinShift,
		  data->dataset[i].adcBits,
		  data->dataset[i].nShots);

	  if(data->dataset[i].deviceId[1]=='T')
	    fprintf(file, "%5.3f ", data->dataset[i].discrLevel);
	  else if(data->dataset[i].deviceId[1]=='C')
	    fprintf(file, "%6.4f ", data->dataset[i].discrLevel);

	  fprintf(file, "%s%x%c%c", data->dataset[i].deviceId,
		  data->dataset[i].nCrate,
		  0xd, 0xa);
	}

      fprintf(file, "%c%c",0xd, 0xa);
      // пишем данные
      for(int i=0; i<data->nDatasets; i++)
	{
	  fwrite(data->dataset[i].data, sizeof(int), data->dataset[i].nDataPoints, file);
	  fprintf(file, "%c%c", 0xd, 0xa);
	}
      
      fclose(file);
      return 0;
    }
  else
    return 1;
}

void printChannel(struct LicelFile *data, int ch)
{
  for(int i=0; i<10; i++)
    printf("%d\n", data->dataset[ch].data[i]);
}

int filterDataset(struct LicelFile *data, int chNum)
{
  int tmp[MAX_LENGTH];

  // фильтруем только аналоговые каналы
  if(!data->dataset[chNum].isPhoton)
    {
      memcpy(tmp, data->dataset[chNum].data, sizeof(int)*data->dataset[chNum].nDataPoints);
      
      medfilt1(tmp, data->dataset[chNum].data, data->dataset[chNum].nDataPoints);
    }
  return 0;
}

