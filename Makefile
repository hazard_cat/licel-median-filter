
CC=gcc
CFLAGS=-Wall -c 
SOURCES=main.c licel.c medianfilter.c
EXECUTABLE=licel-median-filter
OBJECTS=$(SOURCES:.c=.o)
LDFLAGS=

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@


.c.o:
	$(CC) $(CFLAGS) $< -o $@
clean:
	rm *.o
	
